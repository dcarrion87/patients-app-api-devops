"""
Views for the user API.
"""
from .compare import face_comparing
from rest_framework import generics, authentication, permissions
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from django.core.files.storage import FileSystemStorage

from rest_framework.decorators import action,parser_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import MultiPartParser,FormParser

#from .compare import face_comparing
from base64 import urlsafe_b64decode, urlsafe_b64encode
from django.contrib.auth.tokens import default_token_generator
from django.template.loader import render_to_string
from threading import Thread

from django.contrib.auth import (
    get_user_model,
    authenticate,
)
from django.conf import settings
from .mail_verify import get_email_token, get_random_string, send_mail,verify_token,verify_key

from user.serializers import (
    DigitKeyVerifySerializer,
    EmailVerifiySerializer,
    FaceCompareTestSerializer,
    UserSerializer,
    AuthTokenSerializer,
    ChangePasswordSerializer
)


class CreateUserView(generics.CreateAPIView):
    """Create a new user in the system."""
    serializer_class = UserSerializer


class CreateTokenView(ObtainAuthToken):
    """Create a new auth token for user."""
    serializer_class = AuthTokenSerializer
    # added colorfull api views
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class ManageUserView(generics.RetrieveUpdateAPIView):
    """Manage the authenticated user."""
    serializer_class = UserSerializer
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self):
        """Retrieve and return the authenticated user."""
        return self.request.user

class ChangePasswordView(generics.UpdateAPIView):
    """An endpoint for changing password"""
    serializer_class = ChangePasswordSerializer
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    def get_object(self,queryset = None):
        return self.request.user
    def update(self,request,*args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)
        serializer.save()
        response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

        return Response(response)

class EmailVerifyView(generics.CreateAPIView):
    serializer_class = EmailVerifiySerializer
    def create(self,request):
        email= request.data['email']

        users = get_user_model().objects.filter(email = email)
        random_key = get_random_string(6)
        if  users.count() == 0 :
            return Response({"key" : "The user not sign up" ,"status" : False})

        else :
            from_Email = settings.EMAIL_FROM

            data = {
                "email" : email,
                "verify_key" : random_key
            }

            serializer = self.get_serializer(data=data)
            user_ = serializer.save(data)
            send_mail(data)
            return Response({"key" :random_key ,"status" : True},status = status.HTTP_201_CREATED)

class KeyVerifyView(generics.CreateAPIView):
    serializer_class = DigitKeyVerifySerializer
    def create(self,request):
        email= request.data['email']
        key = request.data['key']
        data = {
            "email" : email,
            "verify_key" : key
        }
        flag = verify_key(data)
        return Response({"status" : flag}, status=status.HTTP_201_CREATED)

# check receive email_token and activate user


@api_view(['GET'])
def activate(request, email_token):
    flag = verify_token(email_token)
    if flag:
        return Response({"message":
                        'Thanks for email confirmation. You can login.',
                         "status": True}, status=status.HTTP_201_CREATED)
    else:
        return Response({"message": 'Activation link is invalid!',
                        "status": False})


class FaceCompareView(generics.CreateAPIView):

    serializer_class = FaceCompareTestSerializer

    @parser_classes([FormParser])
    def create(self, request):


        upload = request.FILES['image1']
        fss = FileSystemStorage()
        file = fss.save(upload.name, upload)
        file_url =  settings.BASE_URL  + fss.url(file)
        #return Response({"T": file_url}, status=status.HTTP_201_CREATED)

        cp = face_comparing(file_url)

        return Response({"T": cp}, status=status.HTTP_201_CREATED)

        '''
        users = get_user_model().objects.filter(id = cp)

        if users.count() > 0:
          return Response({"T": cp}, status=status.HTTP_201_CREATED)
        else :
             return Response({"T": -1}, status=status.HTTP_201_CREATED)
        '''