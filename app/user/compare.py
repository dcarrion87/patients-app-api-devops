#from facepplib import FacePP ,exceptions
from .preprocess_image import preprocess_image
from .model_tools import FacialRecognitionModel
import numpy as np
import os
from urllib.request import urlopen
import cv2


from  patients.serializers import *
from django.conf import settings

from core.models import Patients
# define face comparing function
def face_comparing(Image1):
    try:
        model = FacialRecognitionModel()
        model.load_index()

        image = preprocess_image(Image1)

        prediction = model.lookup_image(image)

        predict_id = prediction["Rank 1"]

        return predict_id
    except:
        return -1

    '''
    patient = Patients.objects.filter(id = predict_id)

    print(patient.values())
    flag = False
    lists = patient.values()[0]["image_lists"]
    print(lists)
    for item in lists:
            imagepath = settings.BASE_URL  +item["image"]
            flag = face_comparing(imagepath,Image1)
            if flag == True:
                break
    if flag == True:
        return predict_id
    return -1
    '''

    #return prediction



def faces_to_index(Image1, id):
    model = FacialRecognitionModel()
    model.load_index()

    image_list = []
    label_list = []
    try:
        image = preprocess_image(Image1)
    except:
        raise ValueError(f'Could not add Image because of some reason\n{Image1}' )
    id = int(id)

    model.add_to_index(image, id)
    model.get_index_summary()
