"""
Serializers for the user API View.
"""
from django.contrib.auth import (
    get_user_model,
    authenticate,
)
from django.utils.translation import gettext as _
from django.contrib.auth.models import User

from rest_framework import serializers
from .mail_verify import get_email_token, get_random_string, send_mail, verify_token,verify_key

class UserSerializer(serializers.ModelSerializer):
    """Serializer for the user object."""

    class Meta:
        model = get_user_model()
        fields = ['email',
                  'password',
                  'first_name',
                  'last_name' ,
                  'department_id',
                  'gender',
                  'role',
                  ]
        extra_kwargs = {'password': {'write_only': True, 'min_length': 5}}

    def create(self, validated_data):
        """Create and return a user with encrypted password."""
        get_user_model().objects.create_user(**validated_data)
        random_key = get_random_string(6)
        users = get_user_model().objects.filter(email = validated_data["email"])
        user = users[0]
        print(user)
        user.verify_key = random_key
        user.save()
        data = {
            "email" :  validated_data["email"],
            "verify_key" : random_key
        }
        send_mail(data)
        return user

    def update(self, instance, validated_data):
        """Update and return user."""
        password = validated_data.pop('password', None)
        user = super().update(instance, validated_data)

        if password:
            user.set_password(password)
            user.save()

        return user


class AuthTokenSerializer(serializers.Serializer):
    """Serializer for the user auth token."""
    email = serializers.EmailField()
    password = serializers.CharField(
        style={'input_type': 'password'},
        trim_whitespace=False,
    )

    def validate(self, attrs):
        """Validate and authenticate the user."""
        email = attrs.get('email')
        password = attrs.get('password')


        user = authenticate(
            request=self.context.get('request'),
            username=email,
            password=password,
        )
        if not user:
            msg = _('Unable to authenticate with provided credentials.')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs


class ChangePasswordSerializer(serializers.Serializer):
    model = User

    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
    new_password_confirm = serializers.CharField(required = True)


    def validate_old_password(self,value):
        user = self.context['request'].user
        if not user.check_password(value):
            raise serializers.ValidationError(
                _('Your old password was entered incorrectly. Please enter it again.')
            )
        return value

    def validate(self,data):
        if data['new_password'] != data['new_password_confirm']:
             raise serializers.ValidationError({'new_password_confirm': _("The two password fields didn't match.")})
        return data
    def save(self, **kwargs):
        password = self.validated_data['new_password']
        user = self.context['request'].user
        user.set_password(password)
        user.save()
        return user



class EmailVerifiySerializer(serializers.Serializer):
    model = User
    email = serializers.EmailField(required=True)

    def save(self, data):
      key = data["verify_key"]
      email = data['email']

      users = get_user_model().objects.filter(email = email)
      user = users[0]
      print(user)
      user.verify_key = key
      user.save()
      return user

class DigitKeyVerifySerializer(serializers.Serializer):
    model = User
    email = serializers.EmailField(required=True)
    key = serializers.CharField(required=True)



class FaceCompareTestSerializer(serializers.Serializer):

    image1 = serializers.ImageField(required = True)
    #image2 = serializers.CharField(required = True)
